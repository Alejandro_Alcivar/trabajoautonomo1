package edu.uoc.android.restservice.ui.enter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.model.Owner;

/**
 * Created by edgardopanchana on 4/29/18.
 */

public class AdaptadorFollowers extends RecyclerView.Adapter<AdaptadorFollowers.ViewHolderFollowers> {

    ArrayList<Owner> listaFollowers;

    public AdaptadorFollowers(ArrayList<Owner> listaFollowers) {
        this.listaFollowers = listaFollowers;
    }

    @NonNull
    @Override
    public ViewHolderFollowers onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        return new ViewHolderFollowers(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderFollowers holder, int position) {
        holder.etiNombre.setText(listaFollowers.get(position).getLogin());


        // Aqui se declara de libreria se tendran que tomar las imagenes de los seguidores
        // recordando que utilizaremos "Picasso" como libreria externa para mostrar imagenes

        Picasso.get().load(listaFollowers.get(position).getAvatarUrl()).into(holder.foto);
    }

    @Override
    public int getItemCount() {
        return listaFollowers.size();
    }

    public class ViewHolderFollowers extends RecyclerView.ViewHolder {

        TextView etiNombre;

        // Se debe agregar el nombre de la foto para que no de error en la libreria de la imagen

        ImageView foto;

        public ViewHolderFollowers(View itemView) {
            super(itemView);

            etiNombre = (TextView) itemView.findViewById(R.id.textViewLista);

            // Y se tendra que declarar previamente

            foto = (ImageView) itemView.findViewById(R.id.imageViewLista);
        }
    }
}
